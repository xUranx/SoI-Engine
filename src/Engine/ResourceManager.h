#pragma once
#include <Engine/TextureCahce.h>
#include <Engine/GLTexture.h>
namespace Engine {
	class ResourceManager
	{
	public:
		//static GLuint boundTexture;
		static GLTexture getTexture(std::string textuPath);
	private:
		static TextureCahce textureCache;
	};
}
