#pragma once
#include <Engine/Debug.h>
#if defined(WIN32) && !defined(_FES)
#include <GL/glew.h>
#elif defined(__ANDROID__) || defined(_FES)
#include <GLES3/gl3.h>
#endif
#include <Engine/Vertex.h>
#include <Engine/GLTexture.h>
#include <string>
namespace Engine {

	class Sprite
	{
	public:
		Sprite();
		~Sprite();

		void init(float _x, float _y, float _width, float _height, std::string texturePath);
		void draw();
	private:
		float x, y, width, height;
		GLuint vboID;
		GLTexture textu;

	};

}