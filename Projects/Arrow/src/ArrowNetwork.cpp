#include "ArrowNetwork.h"
#include <Raknet/RakPeerInterface.h>
#include <Raknet/BitStream.h>
#include <Raknet/GetTime.h>
#include <Engine/Log.h>
#include <RakNet/RakNetDefines.h>

ArrowClient::ArrowClient()
{
	n_input.jump = 0;
	n_input.movement = 0;
	n_input.shot = 0;
	n_input.m_shot.angle = 0.0f;
	n_input.m_shot.power = 0.0;

}


ArrowClient::~ArrowClient()
{
}

void ArrowClient::Send(RakNet::RakPeerInterface* peer)
{
	//unsigned char timeS= ID_TIMESTAMP;
	//stream.Write(true);
	//RakNet::Time timeStamp = RakNet::GetTime();
	//stream.Write(timeStamp);
	RakNet::MessageID typeID = ID_INPUT;
	RakNet::BitStream stream;
	stream.Write(typeID);
	stream.Write(n_input.jump);
	stream.Write(n_input.movement);
	stream.Write(n_input.playerID);
	stream.Write(n_input.shot);
	if (n_input.shot)
	{
		stream.Write(n_input.m_shot.angle);
		stream.Write(n_input.m_shot.power);
	}
	peer->Send(&stream, HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, peer->GetGUIDFromIndex(0), false);
	
}

void ArrowClient::Receive(RakNet::Packet *packet, RakNet::RakPeerInterface* peer)
{
	RakNet::BitStream stream(packet->data, packet->length, false);
	RakNet::MessageID typeID;
	stream.Read(typeID);
	if (typeID == ID_POSITIONS)
	{
		players.clear();
		arrows.clear();
		int pSize = 0;
		stream.Read(pSize);
		for (int i = 0; i < pSize; i++)
		{
			PPlayer* pl = new PPlayer;
			stream.Read(pl->id);
			stream.Read(pl->Pos.x);
			stream.Read(pl->Pos.y);
			stream.Read(pl->angle);
			//Engine::Message("Player: %i, X: %f, Y: %f \n", pl->id, pl->Pos.x, pl->Pos.y);
			players.push_back(pl);
		}
		int aSize = 0;
		stream.Read(aSize);
		for (int i = 0; i < aSize; i++)
		{
			PArrow* ar = new PArrow;
			stream.Read(ar->Pos.x);
			stream.Read(ar->Pos.y);
			stream.Read(ar->angle);
			arrows.push_back(ar);
		}
	}
	//m_peer->DeallocatePacket(packet);
}

void ArrowClient::Draw(Engine::GLSpriteBatch& batch)
{

}




ArrowServer::ArrowServer()
{
}

ArrowServer::~ArrowServer()
{
}

void ArrowServer::Send(RakNet::RakPeerInterface* peer)
{
	RakNet::MessageID typeID = ID_POSITIONS;
	RakNet::BitStream stream;
	stream.Write(typeID);
	stream.Write((int)players.size());
	for (auto p : players)
	{
		stream.Write(p->id);
		stream.Write(p->Pos.x);
		stream.Write(p->Pos.y);
		stream.Write(p->angle);
	}
	stream.Write((int)arrows.size());
	for (auto a : arrows)
	{
		stream.Write(a->Pos.x);
		stream.Write(a->Pos.y);
		stream.Write(a->angle);
	}
	for (int i = 0; i < peer->NumberOfConnections(); i++)
	{
		peer->Send(&stream, HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, peer->GetGUIDFromIndex(i), false);
	}

}

void ArrowServer::Receive(RakNet::Packet *packet, RakNet::RakPeerInterface* peer)
{
	RakNet::BitStream stream(packet->data, packet->length,false);
	RakNet::MessageID typeID;
	stream.Read(typeID);
	int i = 0;
	if (typeID == ID_INPUT)
	{
		for (auto p : m_clients)
		{
			if (packet->systemAddress == p.second)
			{
				break;
			}
			i++;
		}
		if (PlayerInput.size() != 0)
		{
			PlayerInput[0]->oldjump = PlayerInput[0]->jump;
			stream.Read(PlayerInput[0]->jump);
			stream.Read(PlayerInput[0]->movement);
			stream.Read(PlayerInput[0]->playerID);
			PlayerInput[0]->oldshot = PlayerInput[0]->shot;
			stream.Read(PlayerInput[0]->shot);
			if (PlayerInput[0]->shot)
			{
				stream.Read(PlayerInput[0]->m_shot.angle);
				stream.Read(PlayerInput[0]->m_shot.power);
			}
			//Engine::Message("Jump: %i, Movement: %i, PlayerID: %i Shot: %i\n", PlayerInput[0]->jump, PlayerInput[0]->movement, PlayerInput[0]->playerID, PlayerInput[0]->shot);
		}
		
		
	}
	//m_peer->DeallocatePacket(packet);
}

bool ArrowServer::checkConnections(RakNet::RakPeerInterface* peer)
{
	int num = peer->NumberOfConnections();

	if (m_client_num < num)
	{
		m_clients.insert(std::pair<int, RakNet::SystemAddress>(m_client_num, peer->GetSystemAddressFromIndex(m_client_num)));
		m_client_num = num;
		Engine::Message("New PLayer!\n");
		return true;
	}
	return false;
	/*RakNet::SystemAddress *addr;
	int newPlayerCount = m_peer->NumberOfConnections();
	std::map<int, RakNet::SystemAddress> t_clients;
	if (newPlayerCount > m_client_num)
	{
		for (int news = m_client_num; news < newPlayerCount; news++)
		{
			bool isThere = false;

			for (int i = 0; i < newPlayerCount; i++)
			{
				for (auto adr : m_clients)
				{
					if (adr.second != m_peer->GetSystemAddressFromIndex(i))
					{
						if(m_freeIDs.size() == 0)
							m_clients.insert(std::pair<int, RakNet::SystemAddress>(news, m_peer->GetSystemAddressFromIndex(i)));
						else
						{
							m_clients.insert(std::pair<int, RakNet::SystemAddress>(m_freeIDs[0], m_peer->GetSystemAddressFromIndex(i)));
							m_freeIDs.erase(m_freeIDs.begin());
						}
					}
				}
			}
		}
	}
	else if(newPlayerCount < m_client_num)
	{
		std::vector<int> toRemove;
		bool isThere = false;
		for (int i = 0; i < m_clients.size(); i++)
		{
			for (int j = 0; j < newPlayerCount; j++)
			{

				if (m_clients[i] == m_peer->GetSystemAddressFromIndex(j))
				{
					bool isThere = true;
				}
			}
			if (!isThere)
			{
				toRemove.push_back(i);
			}
		}
	}
		m_client_num = newPlayerCount;*/
}
