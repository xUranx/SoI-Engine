#include "CollisionListener.h"
#include "Box.h"
#include "Arrow.h"
#include "Player.h"


CollisionListener::CollisionListener()
{
}


CollisionListener::~CollisionListener()
{
}


void CollisionListener::BeginContact(b2Contact * contact)
{
	//check if fixture A has user data
	void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
	bool arrow = false;
	if (bodyUserData) 
	{
		static_cast<GameObj*>(bodyUserData)->startContact();
		static_cast<GameObj*>(bodyUserData)->collision(contact->GetFixtureB());
	}
	//check if fixture A was the foot sensor
	void* fixtureUserData = contact->GetFixtureA()->GetUserData();
	if ((int)fixtureUserData == 3)
	{
		static_cast<Player*>(bodyUserData)->footContact();
	}

	//check if fixture A has user data
	bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
	if (bodyUserData) 
	{
		static_cast<GameObj*>(bodyUserData)->startContact();
		static_cast<GameObj*>(bodyUserData)->collision(contact->GetFixtureA());
	}

	//check if fixture B was the foot sensor
	fixtureUserData = contact->GetFixtureB()->GetUserData();
	if ((int)fixtureUserData == 3)
	{
		static_cast<Player*>(bodyUserData)->footContact();
	}
}

void CollisionListener::EndContact(b2Contact * contact)
{
	//check if fixture A has user data
	void* bodyUserData = contact->GetFixtureA()->GetBody()->GetUserData();
	if (bodyUserData)
	{
		static_cast<GameObj*>(bodyUserData)->endContact();
	}

	void* fixtureUserData = contact->GetFixtureA()->GetUserData();
	if ((int)fixtureUserData == 3)
	{
		static_cast<Player*>(bodyUserData)->footNotContact();
	}
	

	//check if fixture A has user data
	bodyUserData = contact->GetFixtureB()->GetBody()->GetUserData();
	if (bodyUserData) 
	{
		static_cast<GameObj*>(bodyUserData)->endContact();
	}

	fixtureUserData = contact->GetFixtureB()->GetUserData();
	if ((int)fixtureUserData == 3)
	{
		static_cast<Player*>(bodyUserData)->footNotContact();
	}
}
