#pragma once
#include <glm/glm.hpp>
#include <Raknet/MessageIdentifiers.h>
#include <Engine/Network.h>
#include <Engine/SpriteBatch.h>


struct PPlayer
{
	glm::vec2 Pos = glm::vec2(0);
	float angle = 0.0f;
	unsigned int id = 0;
};

struct PArrow
{
	glm::vec2 Pos = glm::vec2(0);
	float angle = 0.0f;
};

struct Shot
{
	float angle = 0.0f;
	float power = 0.0f;
};

struct Input
{
	int movement = 0;
	int jump = 0;
	int oldjump = 1;
	int playerID = 0;
	int shot = 0;
	int oldshot = 1;
	Shot m_shot;
};


enum
{
	ID_INPUT = ID_USER_PACKET_ENUM+1,
	ID_POSITIONS
};
class ArrowClient : public Engine::Client
{
public:
	ArrowClient();
	~ArrowClient();

	void Send(RakNet::RakPeerInterface* peer);
	void Receive(RakNet::Packet *packet, RakNet::RakPeerInterface* peer);
	void Draw(Engine::GLSpriteBatch&);

	Input n_input;

	int my_id;

	std::vector<PPlayer*> players;
	std::vector<PArrow*> arrows;
private:
};

class ArrowServer : public Engine::Server
{
public:
	ArrowServer();
	~ArrowServer();

	void Send(RakNet::RakPeerInterface* peer);
	void Receive(RakNet::Packet *packet, RakNet::RakPeerInterface* peer);

	bool checkConnections(RakNet::RakPeerInterface* peer);

	std::vector<PPlayer*> players;
	std::vector<PArrow*> arrows;

	std::vector<Input*> PlayerInput;
private:
};
